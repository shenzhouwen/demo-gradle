package com.xkw.demo.mapper.test1;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserMapper {
	
	@Select("SELECT uid,username,`password` FROM `user`")
	List<Map<String,Object>> getUserList();

}
