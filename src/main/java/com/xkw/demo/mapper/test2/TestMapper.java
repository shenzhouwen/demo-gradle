package com.xkw.demo.mapper.test2;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface TestMapper {
	
	@Select("SELECT `name`,age,grade FROM test")
	List<Map<String,Object>> getTestList();

}
