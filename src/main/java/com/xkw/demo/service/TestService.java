package com.xkw.demo.service;

import java.util.List;
import java.util.Map;

public interface TestService {
	
	List<Map<String,Object>> getTestList();
}
