package com.xkw.demo.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xkw.demo.mapper.test2.TestMapper;
import com.xkw.demo.service.TestService;

@Service("testService")
public class TestServiceImpl implements TestService {

	@Autowired
	TestMapper testMapper;
	
	@Override
	public List<Map<String, Object>> getTestList() {
		
		return testMapper.getTestList();
	}

}
