package com.xkw.demo.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xkw.demo.mapper.test1.UserMapper;
import com.xkw.demo.service.UserService;

@Service("userService")
public class UserServiceImpl implements UserService {

	@Autowired
	UserMapper userMapper;
	
	@Override
	public List<Map<String, Object>> getUserList() {
		
		return userMapper.getUserList();
	}

}
