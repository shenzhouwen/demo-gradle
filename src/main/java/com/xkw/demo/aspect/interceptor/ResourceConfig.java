package com.xkw.demo.aspect.interceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.util.ResourceUtils;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@EnableWebMvc
@Configuration
public class ResourceConfig implements WebMvcConfigurer {
	/**
	 * 补充：html中需要引用css与js文件时，如果css和js不是直接在static下，而是在static下的文件夹中。在后台需要配置对静态资源的引用，
	 * 否则访问不到资源文件。添加配置后，html中就可以用@{...}来引用static下的js/css/img等资源。
	 * 当然，如果你只提供接口提供数据不涉及页面的话就不需要这些了。
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		//将classpath:下的static中的所有文件视为静态资源
		registry.addResourceHandler("/static/**").addResourceLocations(ResourceUtils.CLASSPATH_URL_PREFIX+"/static/");
	}
	
	/**
	 * 拦截器是在过滤器之后才执行的,一般拦截器中的拦截地址，过滤器是不用处理的。否则就进不到拦截器中
	 */
	public void addInterceptors(InterceptorRegistry registry) {
		//拦截器 当 /admin/api/** 的接口被访问时拦截器开始工作
		registry.addInterceptor(new MyInterceptor()).addPathPatterns("/admin/api/**");
	}

}
