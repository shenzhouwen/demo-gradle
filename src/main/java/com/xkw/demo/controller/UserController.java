package com.xkw.demo.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xkw.demo.config.annontation.MyAnnotation;
import com.xkw.demo.service.UserService;

//如果使用@Controller注解需要在方法上加入@ResponseBody来调用ajax方法
@RestController
public class UserController {
	@Autowired
	private UserService userService;
	
	//@ResponseBody
	@Transactional(rollbackFor = Exception.class)//事务处理
	@RequestMapping("/user_list")
	public List<Map<String, Object>> userList() {
		return userService.getUserList();
	}
	
	//@ResponseBody
	@RequestMapping(value="testAsp", produces = "text/plain;charset=UTF-8")
	@MyAnnotation(operate="切面测试操作", isSave=false)
	public Object testAspect(@RequestParam(name = "name",required=false,defaultValue="佚名") String name) {	
		return name;
	}
}
