package com.xkw.demo.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xkw.demo.service.TestService;
/**
 * @RestController注解相当于@ResponseBody ＋ @Controller
 * 合在一起的作用
 * 1) 如果只是使用@RestController注解Controller，则Controller中的方法无法返回jsp页面，或者html，
 * 配置的视图解析器 InternalResourceViewResolver不起作用，返回的内容就是Return 里的内容。
 * 2) 如果需要返回到指定页面，则需要用 @Controller
 * 配合视图解析器InternalResourceViewResolver才行。 如果需要返回JSON，XML或自定义mediaType内容到页面，
 * 则需要在对应的方法上加上@ResponseBody注解。
 * 例如:使用@Controller 注解，在对应的方法上，视图解析器可以解析return 的jsp,html页面，并且跳转到相应页面若返回json等内容到页面，
 * 则需要加@ResponseBody注解
 * 
 */
@Controller
public class TestController {
	
	@Autowired
	private TestService testService;
	
	@ResponseBody
	@Transactional(rollbackFor = Exception.class)//事务处理
	@RequestMapping("/test_list")
	public List<Map<String, Object>> userList() {
		return testService.getTestList();
	}
	

	@RequestMapping("/hello")
	public Object test(ModelMap map) {
		map.put("msg", "Hello world!");
		return "hello";
	}
}
