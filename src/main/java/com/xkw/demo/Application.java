package com.xkw.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
/**
 * 
 * demo说明地址https://blog.csdn.net/helloworld_in_java/article/details/81519267
 *
 * 导出jar包：在pom.xml文件所在的目录下，
 * 打开命令窗口执行：mvn clean package -Dmaven.test.skip=true
 *jar包会生成到target目录中
 *
 */
@ServletComponentScan//开启过滤器，Servlet、Filter、Listener可以直接通过@WebServlet、@WebFilter、@WebListener注解自动注册
@EnableScheduling//启用定时任务注解
@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	/**
	 * 事务：直接在controller对应的方法中加入@Transactional注解即可
	 * 
	 * 需要注意的是，如果mysql 事务未提交导致死锁 Lock wait timeout exceeded; try restarting transaction 解决办法是：
	 * 1.先用这条命令查询数据库阻塞的进程
	 * SELECT * FROM information_schema.innodb_trx
	 * 看 trx_tables_in_use， trx_tables_locked, trx_rows_locked 这三个值是否为0，不为0的则证明有事务未提交。
	 * 此时找到该行对应 trx_mysql_thread_id, 记录。
	 * 2.然后使用kill命令关闭该事务即可、kill   id  ; (杀死第一步找到的trx_mysql_thread_id的进程)
	 */

}
