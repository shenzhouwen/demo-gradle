package com.xkw.demo.config.filter;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/*
 * 注意此时LoginFilter不需要用@WebFilter注解，主类也不需要加上@ServletComponentScan注解
 * 多个过滤器时推荐使用该种过滤器配置
 */
@Configuration
public class FilterConfig {
    @Bean
    public FilterRegistrationBean<LoginFilter> filterRegist() {
        FilterRegistrationBean<LoginFilter> frBean = new FilterRegistrationBean<LoginFilter>();
        frBean.setFilter(new LoginFilter());
        frBean.addUrlPatterns("/*");
        frBean.setOrder(1);    //order值越小越先加载
        return frBean;
    }
}