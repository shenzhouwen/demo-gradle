package com.xkw.demo.scheduled;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 在定时任务类中加入component注解让spring找到，
 * 然后在需要定时执行的方法上加上@Scheduled，
 * 启动类上加入@EnableScheduling启用定时任务注解
 */
@Component
public class MyScheduledTask {

	/**
	 * 定时任务执行周期为5秒钟
	 */
	@Scheduled(cron="*/5 * * * * *")
	public void mysScheduledMethod() {
		System.out.println("-------------------- cron task ------------------------");
	}
}
